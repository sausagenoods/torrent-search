package leetx

import (
	"fmt"

	ts "gitlab.com/sausagenoods/torrentsearch"
)

var leetxBaseUrl = "https://1337x.to"
var searchPath = "/search/%s/%d/"
var categorySearchPath = "/category-search/%s/%s/%d/"

// Search without specifying category
func Search(s *ts.Scraper, query string) ([]ts.Torrent, error) {
	resp, err := s.Client.Get(fmt.Sprintf(leetxBaseUrl + searchPath,
	    query, 1))
	if err != nil {
		return nil, err
	}
	return ParseTable(s, resp.Body)
}

func categorySearch(s *ts.Scraper, category, query string) ([]ts.Torrent, error) {
	resp, err := s.Client.Get(fmt.Sprintf(leetxBaseUrl + categorySearchPath,
	    query, category, 1))
	if err != nil {
		return nil, err
	}
	return ParseTable(s, resp.Body)
}

// Search under anime category.
func AnimeSearch(s *ts.Scraper, query string) ([]ts.Torrent, error) {
	return categorySearch(s, "Anime", query)
}

// Search under applications category.
func AppSearch(s *ts.Scraper, query string) ([]ts.Torrent, error) {
	return categorySearch(s, "Apps", query)
}

// Search under documentaries category.
func DocumentarySearch(s *ts.Scraper, query string) ([]ts.Torrent, error) {
	return categorySearch(s, "Documentaries", query)
}

// Search under games category.
func GameSearch(s *ts.Scraper, query string) ([]ts.Torrent, error) {
	return categorySearch(s, "Games", query)
}

// Search under movies category.
func MovieSearch(s *ts.Scraper, query string) ([]ts.Torrent, error) {
	return categorySearch(s, "Movies", query)
}

// Search under music category.
func MusicSearch(s *ts.Scraper, query string) ([]ts.Torrent, error) {
	return categorySearch(s, "Music", query)
}

// Search under other category.
func OtherSearch(s *ts.Scraper, query string) ([]ts.Torrent, error) {
	return categorySearch(s, "Other", query)
}

// Search under television category.
func TVSearch(s *ts.Scraper, query string) ([]ts.Torrent, error) {
	return categorySearch(s, "TV", query)
}

// Search under XXX category.
func XXXSearch(s *ts.Scraper, query string) ([]ts.Torrent, error) {
	return categorySearch(s, "XXX", query)
}
