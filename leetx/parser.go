package leetx

import (
	"io"
	"regexp"

	ts "gitlab.com/sausagenoods/torrentsearch"
	"github.com/PuerkitoBio/goquery"
)

func GetMagnetLink(s *ts.Scraper, pageUrl string) (string, error) {
	resp, err := s.Client.Get(pageUrl)
	if err != nil {
		return "", err
	}
	b, err := io.ReadAll(resp.Body)
	if err != nil {
		return "", err
	}
	re := regexp.MustCompile(`"magnet:\S+"`)
	match := re.FindStringSubmatch(string(b))
	return match[0], nil
}

func ParseTable(s *ts.Scraper, r io.Reader) ([]ts.Torrent, error) {
	doc, err := goquery.NewDocumentFromReader(r)
	if err != nil {
		return nil, err
	}

	var torrents []ts.Torrent

	doc.Find("tbody tr").Each(func(i int, sel *goquery.Selection) {
		var t ts.Torrent
		title := sel.Find(".coll-1.name a").Eq(1)
		t.Title = title.Text()
		u, _ := title.Attr("href")
		t.MagnetLink, err = GetMagnetLink(s, leetxBaseUrl + u)
		torrents = append(torrents, t)
	})

	return torrents, err
}
