package torrentsearch

import (
	"time"
	"net/http"
)

type Torrent struct {
	Title string
	Seeds uint
	Leeches uint
	// Upload date
	Date time.Time
	// Size in Megabytes
	Size uint
	Uploader string
	MagnetLink string
}

type Scraper struct {
	Client *http.Client
	// TODO: UserAgents/proxy list, timeouts
}

func New() *Scraper {
	c := &http.Client{Timeout: 5 * time.Second}
	return &Scraper{Client: c}
}
