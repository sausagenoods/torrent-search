package main

import (
	"fmt"
	"log"

	ts "gitlab.com/sausagenoods/torrentsearch"
	"gitlab.com/sausagenoods/torrentsearch/leetx"
)

func main() {
	s := ts.New()
	t, err := leetx.MusicSearch(s, "Gorillaz")
	if err != nil {
		log.Println(err)
	}
	for _, i := range t {
		fmt.Printf("Title: %.30s...\t Link: %.30s...\n", i.Title, i.MagnetLink)
	}
}
